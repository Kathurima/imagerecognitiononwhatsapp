# ImageRecognitionOnWhatsapp

Data Visualization with WhatsApp, Google Maps, Python, Twilio, and Clarifai

A digital board to visualize where friends are in the world and how we’re all still connected under the same sky.
